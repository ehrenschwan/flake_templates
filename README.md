# nix flake templates

This is my personal collection of nix flake templates that.

## Flakes

### Todo

- [ ] rust_bevy
- [ ] some simple node/typescript

### Rust

A flake that builds a rust project.

### Rust Docker

A flake that builds a rust project into a docker image.

TODOS:
- [ ] make it just an extension of the rust flake

## Usage

To use the templates either use 

```sh
nix flake init -t git+https://git.ehrenschwan.org/ehrenschwan/flake_templates#<template>
```

or 

```sh
nix flake new -t git+https://git.ehrenschwan.org/ehrenschwan/flake_templates#<template> <folder_name>
```
