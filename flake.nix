{
  description = "A collection of my flakes";

  inputs = { nixpkgs.url = "github:nixos/nixpkgs/nixos-unstable"; };

  outputs = { self, nixpkgs }:
    let
      system = "x86_64-linux";
      pkgs = import nixpkgs { inherit system; };
    in {
      templates = {
        rust = { path = ./rust; };
        rustDocker = { path = ./rust_docker; };
        rustDioxus = { path = ./rust_dioxus; };
      };

      devShells.${system}.default = pkgs.mkShell {
        buildInputs = with pkgs; [
          nil
          nixfmt
          nodePackages.bash-language-server
          shfmt
          shellcheck
        ];
      };
    };
}
