{
  description =
    "A rust flake that uses fenix for the toolchain and crane for building";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    fenix = {
      url = "github:nix-community/fenix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
    crane = {
      url = "github:ipetkov/crane";
      inputs = { nixpkgs.follows = "nixpkgs"; };
    };
  };

  outputs = { self, nixpkgs, flake-utils, fenix, crane, ... }:
    flake-utils.lib.eachDefaultSystem (system:
      let
        overlays = [ fenix.overlays.default ];

        pkgs = import nixpkgs {
          inherit system overlays;
          config.allowUnfree = true;
        };

        rustPkg = pkgs.fenix.fromToolchainFile {
          file = ./rust-toolchain.toml;
          sha256 = "sha256-ufmGU3VVScQdwmKFMQKZgfn1WambE4o1ZifQGAq7g0s";
        };

        craneLib = (crane.mkLib pkgs).overrideToolchain rustPkg;
        src = craneLib.cleanCargoSource (craneLib.path ./.);

        commonArgs = {
          inherit src;
          strictDeps = true;
        };

        cargoArtifacts = craneLib.buildDepsOnly commonArgs;

        my-package =
          craneLib.buildPackage (commonArgs // { inherit cargoArtifacts; });
      in {
        checks = { inherit my-package; };
        packages.default = my-package;
        devShells.default = craneLib.devShell {
          checks = self.checks.${system};

          packages = with pkgs; [ nixfmt-classic nil ];

          inputsFrom = [ my-package ];

          RUST_BACKTRACE = "1";
          RUST_LOG = "info";
          RUST_SRC_PATH = "${rustPkg}/lib/rustlib/src/rust/library";
        };
      });
}
