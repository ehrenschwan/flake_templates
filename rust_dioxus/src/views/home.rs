use dioxus::prelude::*;
use crate::components::{Hero, Echo};

#[component]
pub fn Home() -> Element {
    rsx! {
        Hero {}
        Echo {}
    }
}
